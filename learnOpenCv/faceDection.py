import cv2


# loading the cascade
facecascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye=cv2.CascadeClassifier('haarcascade_eye.xml')

# defining the function to do detection

def detect(gray,frame):
    faces=facecascade.detectMultiScale(gray,1.3,5)
    for(x,y,w,h) in faces:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        rol_gray=gray[y:y+h,x:x+w]
        rol_color=frame[y:y+h,x:x+w]
        eyes = eye.detectMultiScale(rol_gray, 1.1, 5)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(rol_color, (ex, ey), (ex + ew, ey + eh), (255, 100, 0), 2)
    return frame



video_capture=cv2.VideoCapture(0)

while True:
    _ ,frame=video_capture.read()
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    canvas=detect(gray,frame)
    cv2.imshow('video',canvas)
    if cv2.waitKey(1) & 0xff==ord('q'):
        break
video_capture.release()
cv2.destroyAllWindows()







